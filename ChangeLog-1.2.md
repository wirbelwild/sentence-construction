# Changes in Bit&Black SentenceConstruction v1.2

## 1.2.2 2021-04-01

### Changed

-   Improved array types.

## 1.2.1 2020-11-25

### Added

-   Added support for PHP 8.