[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/sentence-construction)](http://www.php.net)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/89d2a75c3e494ae28ecc22e7232b5a29)](https://www.codacy.com/bb/wirbelwild/sentence-construction/dashboard) 
[![Latest Stable Version](https://poser.pugx.org/bitandblack/sentence-construction/v/stable)](https://packagist.org/packages/bitandblack/sentence-construction)
[![Total Downloads](https://poser.pugx.org/bitandblack/sentence-construction/downloads)](https://packagist.org/packages/bitandblack/sentence-construction)
[![License](https://poser.pugx.org/bitandblack/sentence-construction/license)](https://packagist.org/packages/bitandblack/sentence-construction)

# Sentence construction

Creates nice sentences.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/sentence-construction). Add it to your project by running `$ composer require bitandblack/sentence-construction`.

## Usage 

Create a sentence like that:

```php
<?php

use BitAndBlack\SentenceConstruction;

$fruits = [
    'Apples', 
    'Bananas', 
    'Pears'
];

$sentence = new SentenceConstruction(
    glue: ', ', 
    glueLast: ' and ', 
    pieces: $fruits,
);

var_dump('I like ' . $sentence);
```

This will dump `I like Apples, Bananas and Pears`.

## Help

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).
