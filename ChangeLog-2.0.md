# Changes in Bit&Black SentenceConstruction v2.0

## 2.0.0 2022-06-03

### Changed

-   PHP 8.0 is now required.
-   Removed deprecated methods.