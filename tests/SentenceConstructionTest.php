<?php

/**
 * Bit&Black Sentence Construction.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Tests;

use BitAndBlack\SentenceConstruction;
use Generator;
use PHPUnit\Framework\TestCase;

class SentenceConstructionTest extends TestCase
{
    /**
     * @dataProvider sentenceConstructionProvider
     * @param array<int, string> $pieces
     */
    public function testSentenceConstruction(string $expected, string $glue, string $glueLast, array $pieces): void
    {
        $sentenceConstruction = new SentenceConstruction($glue, $glueLast, $pieces);

        self::assertSame(
            $expected,
            $sentenceConstruction->getSentence()
        );
    }

    public static function sentenceConstructionProvider(): Generator
    {
        yield 'single word' => [
            'word',
            ', ',
            ' and ',
            [
                'word',
            ],
        ];

        yield 'two words' => [
            'word1 and word2',
            ', ',
            ' and ',
            [
                'word1',
                'word2',
            ],
        ];

        yield 'three words' => [
            'word1, word2 and word3',
            ', ',
            ' and ',
            [
                'word1',
                'word2',
                'word3',
            ],
        ];

        yield 'four words' => [
            'word1, word2, word3 and word4',
            ', ',
            ' and ',
            [
                'word1',
                'word2',
                'word3',
                'word4',
            ],
        ];

        yield 'int' => [
            '1',
            ', ',
            ' and ',
            [
                1,
            ],
        ];

        yield 'float' => [
            '1.5',
            ', ',
            ' and ',
            [
                1.5,
            ],
        ];

        yield 'empty array' => [
            '',
            ', ',
            ' and ',
            [],
        ];
    }
}
