<?php

/**
 * Bit&Black Sentence Construction.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack;

use Stringable;

/**
 * The SentenceConstruction class returns a sentence from words in an array.
 *
 * @package BitAndBlack
 * @see \BitAndBlack\Tests\SentenceConstructionTest
 */
class SentenceConstruction implements Stringable
{
    private string $sentence;

    /**
     * SentenceConstruction Constructor.
     *
     * @param string $glue                    The string which will be added between each array item.
     *                                        This should mostly be a comma followed by a whitespace `, `.
     * @param string $glueLast                The string which will be added at the last position.
     *                                        This should be something like ` and ` or ` or `.
     * @param array<string|int|float> $pieces All items.
     */
    public function __construct(string $glue, string $glueLast, array $pieces)
    {
        $pieces = array_values($pieces);

        if (1 === count($pieces)) {
            $this->sentence = (string) $pieces[0];
            return;
        }

        if (empty($pieces)) {
            $this->sentence = '';
            return;
        }

        $pieceLast = array_pop($pieces);
        $sentence = implode($glue, $pieces);
        $sentence .= $glueLast . $pieceLast;

        $this->sentence = $sentence;
    }

    public function __toString(): string
    {
        return $this->getSentence();
    }

    /**
     * Returns the whole sentence.
     */
    public function getSentence(): string
    {
        return $this->sentence;
    }
}
